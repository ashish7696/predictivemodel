import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirstScreenComponent } from './first-screen/first-screen.component';
import { SecondScreenComponent } from './second-screen/second-screen.component';
import { ThirdScreenComponent } from './third-screen/third-screen.component';
import {RouterModule} from '@angular/router';
import {MAIN_ROUTES} from './app.route';
import { GraphScreenComponent } from './graph-screen/graph-screen.component';
import {ChartModule , ColumnSeriesService} from '@syncfusion/ej2-angular-charts';
import { ProjectionFirstComponent } from './projection_first_screen/projection-first.component';
import { ProjectionSecondComponent } from './projection_second_screen/projection-second.component';
import { ProjectionThirdScreenComponent } from './projection_third_screen/projection-third-screen.component';




@NgModule({
  declarations: [
    AppComponent,
    FirstScreenComponent,
    SecondScreenComponent,
    ThirdScreenComponent,
    GraphScreenComponent,
    ProjectionFirstComponent,
    ProjectionSecondComponent,
    ProjectionThirdScreenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(MAIN_ROUTES),
   ChartModule,

  ],
  providers: [ColumnSeriesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
