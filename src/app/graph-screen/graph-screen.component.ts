import {Component, Input, OnInit} from '@angular/core';

import * as CanvasJS from './canvasjs.min';

@Component({
  selector: 'app-graph-screen',
  templateUrl: './graph-screen.component.html',
  styleUrls: ['./graph-screen.component.scss']
})
export class GraphScreenComponent implements OnInit {

  @Input() childMessage: number;

  constructor() { }

  ngOnInit() {

    let chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: true,
      title: {
        text: "Alpha VS Beta"
      },
      data: [{
        type: "column",
        dataPoints: [
          {y: 15, label: "Alpha"},
          {y: 10, label: "Beta"}
        ]
      }]
    });

    chart.render();
  }





}
