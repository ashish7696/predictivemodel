import {Routes} from '@angular/router';
import {FirstScreenComponent} from './first-screen/first-screen.component';
import {SecondScreenComponent} from './second-screen/second-screen.component';
import {ThirdScreenComponent} from './third-screen/third-screen.component';
import {GraphScreenComponent} from './graph-screen/graph-screen.component';
import {ProjectionFirstComponent} from "./projection_first_screen/projection-first.component";
import {ProjectionSecondComponent} from "./projection_second_screen/projection-second.component";
import {ProjectionThirdScreenComponent} from "./projection_third_screen/projection-third-screen.component";




export const MAIN_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'firstscreen',
    pathMatch: 'full',

  },
  {
    path: 'firstscreen',
    component: FirstScreenComponent,
  },
  {
    path: 'secondscreen',
    component: SecondScreenComponent,
  },
  {
    path: 'thirdscreen',
    component: ThirdScreenComponent,
  },
  {
    path: 'graphscreen',
    component: GraphScreenComponent,
  },
  {
    path: '**',
    redirectTo: 'firstscreen'
  },
  {
    path: 'projection_first_screen',
    component: ProjectionFirstComponent,
  },
  {
    path: 'projection_second_screen',
    component: ProjectionSecondComponent,
  },
  {
    path: 'projection_third_screen',
    component: ProjectionThirdScreenComponent,
  },
];
