import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectionThirdScreenComponent } from './projection-third-screen.component';

describe('ProjectionThirdScreenComponent', () => {
  let component: ProjectionThirdScreenComponent;
  let fixture: ComponentFixture<ProjectionThirdScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectionThirdScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectionThirdScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
