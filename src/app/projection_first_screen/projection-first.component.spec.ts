import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectionFirstComponent } from './projection-first.component';

describe('ProjectionFirstComponent', () => {
  let component: ProjectionFirstComponent;
  let fixture: ComponentFixture<ProjectionFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectionFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectionFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
