import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectionSecondComponent } from './projection-second.component';

describe('ProjectionSecondComponent', () => {
  let component: ProjectionSecondComponent;
  let fixture: ComponentFixture<ProjectionSecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectionSecondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectionSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
